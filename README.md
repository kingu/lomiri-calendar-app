# Lomiri Calendar Application

Calendar App is the official calendar app for Ubuntu Touch (UT) which syncs
with online accounts. Feel free to contribute and get contact, easiest on
Matrix or Telegram or on the
[issue tracker](https://gitlab.com/ubports/development/apps/lomiri-calendar-app/issues).
The calendar app once followed a test driven development (TDD) where tests are
written in parallel to feature implementation to help spot regressions easier.
We "lost" the test during the transition from Canonical, but hopefully we bring
them back soon.

## Original Design Document

The [original design document][1] from Canonical is still available.

[1]: https://docs.google.com/presentation/d/14NIPecPFKb_8Ad3O4suEGJqVOw9bC4n0s63uWalbZ98/edit#slide=id.p

## Launch

This application is implemented in QML/JS.  You need Qt5, the Lomiri UI Toolkit
and the qmlscene viewer in order to launch this application natively on the PC.

    $ cd lomiri-calendar-app
    $ qmlscene qml/calendar.qml

You can install and run it on your UT device using `clickable`. Once you have
installed `clickable` (you can get it with apt/ppa or pip), its as simple as:

    $ cd lomiri-calendar-app
    $ clickable

If you want to try the Calendar App out on the PC, but you don't have the
Lomiri UI Toolkit installed you can run it in the clickable Docker container:

    $ cd lomiri-calendar-app
    $ clickable desktop

## Contributing

All contributions are welcome, please see the information below for details of
how you can help.

### Coding

Please refer to the *README.developers* file for details on how to build the
app and contribute code.

### Translation

You can easily contribute to the localization of this project (i.e. the
translation into your language) by visiting (and signing up with) the
[Hosted Weblate service](https://hosted.weblate.org/projects/lomiri/lomiri-calendar-app)

The localization platform of this project is sponsored by Hosted Weblate
via their free hosting plan for Libre and Open Source Projects.

### Ideas & Bugs
Please create a new issue in our [issue
tracker](https://gitlab.com/ubports/development/apps/lomiri-calendar-app/issues).

### Other

Please go to UBPorts [contribution](https://ubports.com/page/vo-get-involved)
page for more details on other ways to supports UBPorts.
