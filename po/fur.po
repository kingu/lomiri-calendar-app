# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the lomiri-calendar-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: lomiri-calendar-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-04 09:07+0000\n"
"PO-Revision-Date: 2023-01-04 19:46+0000\n"
"Last-Translator: Anonymous <noreply@weblate.org>\n"
"Language-Team: Friulian <https://hosted.weblate.org/projects/lomiri/lomiri-"
"calendar-app/fur/>\n"
"Language: fur\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.15.1-dev\n"

#: lomiri-calendar-app.desktop.in:7
msgid "/usr/share/lomiri-calendar-app/assets/lomiri-calendar-app@30.png"
msgstr ""

#: lomiri-calendar-app.desktop.in:8 qml/EventDetails.qml:348
#: qml/NewEvent.qml:677
msgid "Calendar"
msgstr ""

#: lomiri-calendar-app.desktop.in:9
msgid "A calendar for Lomiri which syncs with online accounts."
msgstr ""

#: lomiri-calendar-app.desktop.in:10
msgid "calendar;event;day;week;year;appointment;meeting;"
msgstr ""

#: qml/AgendaView.qml:51 qml/calendar.qml:373 qml/calendar.qml:554
msgid "Agenda"
msgstr ""

#: qml/AgendaView.qml:101
msgid "You have no calendars enabled"
msgstr ""

#: qml/AgendaView.qml:101
msgid "No upcoming events"
msgstr ""

#: qml/AgendaView.qml:113
msgid "Enable calendars"
msgstr ""

#: qml/AgendaView.qml:206
msgid "no event name set"
msgstr ""

#: qml/AgendaView.qml:208
msgid "no location"
msgstr ""

#: qml/AllDayEventComponent.qml:89 qml/TimeLineBase.qml:50
msgid "New event"
msgstr ""

#. TRANSLATORS: Please keep the translation of this string to a max of
#. 5 characters as the week view where it is shown has limited space.
#: qml/AllDayEventComponent.qml:148
msgid "%1 event"
msgid_plural "%1 events"
msgstr[0] ""
msgstr[1] ""

#. TRANSLATORS: the argument refers to the number of all day events
#: qml/AllDayEventComponent.qml:152
msgid "%1 all day event"
msgid_plural "%1 all day events"
msgstr[0] ""
msgstr[1] ""

#: qml/CalendarChoicePopup.qml:45 qml/EventActions.qml:55
msgid "Calendars"
msgstr ""

#: qml/CalendarChoicePopup.qml:47 qml/SettingsPage.qml:51
msgid "Back"
msgstr ""

#. TRANSLATORS: Please translate this string  to 15 characters only.
#. Currently ,there is no way we can increase width of action menu currently.
#: qml/CalendarChoicePopup.qml:59 qml/EventActions.qml:40
msgid "Sync"
msgstr ""

#: qml/CalendarChoicePopup.qml:59 qml/EventActions.qml:40
msgid "Syncing"
msgstr ""

#: qml/CalendarChoicePopup.qml:84
msgid "Add online Calendar"
msgstr ""

#: qml/CalendarChoicePopup.qml:189
msgid "Unable to deselect"
msgstr ""

#: qml/CalendarChoicePopup.qml:190
msgid ""
"In order to create new events you must have at least one writable calendar "
"selected"
msgstr ""

#: qml/CalendarChoicePopup.qml:192 qml/CalendarChoicePopup.qml:205
msgid "Ok"
msgstr ""

#: qml/CalendarChoicePopup.qml:202
msgid "Network required"
msgstr ""

#: qml/CalendarChoicePopup.qml:203
msgid ""
"You are currently offline. In order to add online accounts you must have "
"network connection available."
msgstr ""

#: qml/ColorPickerDialog.qml:25
msgid "Select Color"
msgstr ""

#: qml/ColorPickerDialog.qml:55 qml/DeleteConfirmationDialog.qml:63
#: qml/EditEventConfirmationDialog.qml:52 qml/NewEvent.qml:394
#: qml/OnlineAccountsHelper.qml:73 qml/RemindersPage.qml:88
#, fuzzy
msgid "Cancel"
msgstr "Cancel"

#: qml/ContactChoicePopup.qml:37
msgid "No contact"
msgstr ""

#: qml/ContactChoicePopup.qml:96
msgid "Search contact"
msgstr ""

#: qml/DayView.qml:76 qml/MonthView.qml:51 qml/WeekView.qml:60
#: qml/YearView.qml:57
msgid "Today"
msgstr ""

#. TRANSLATORS: this is a time formatting string,
#. see http://qt-project.org/doc/qt-5/qml-qtqml-date.html#details for valid expressions.
#. It's used in the header of the month and week views
#: qml/DayView.qml:129 qml/MonthView.qml:84 qml/WeekView.qml:159
msgid "MMMM yyyy"
msgstr ""

#: qml/DeleteConfirmationDialog.qml:31
msgid "Delete Recurring Event"
msgstr ""

#: qml/DeleteConfirmationDialog.qml:32
msgid "Delete Event"
msgstr ""

#. TRANSLATORS: argument (%1) refers to an event name.
#: qml/DeleteConfirmationDialog.qml:36
msgid "Delete only this event \"%1\", or all events in the series?"
msgstr ""

#: qml/DeleteConfirmationDialog.qml:37
msgid "Are you sure you want to delete the event \"%1\"?"
msgstr ""

#: qml/DeleteConfirmationDialog.qml:40
msgid "Delete series"
msgstr ""

#: qml/DeleteConfirmationDialog.qml:51
msgid "Delete this"
msgstr ""

#: qml/DeleteConfirmationDialog.qml:51 qml/NewEvent.qml:401
#, fuzzy
msgid "Delete"
msgstr "Delete"

#: qml/EditEventConfirmationDialog.qml:29 qml/NewEvent.qml:389
msgid "Edit Event"
msgstr ""

#. TRANSLATORS: argument (%1) refers to an event name.
#: qml/EditEventConfirmationDialog.qml:32
msgid "Edit only this event \"%1\", or all events in the series?"
msgstr ""

#: qml/EditEventConfirmationDialog.qml:35
msgid "Edit series"
msgstr ""

#: qml/EditEventConfirmationDialog.qml:44
msgid "Edit this"
msgstr ""

#: qml/EventActions.qml:67 qml/SettingsPage.qml:49
msgid "Settings"
msgstr ""

#. TRANSLATORS: the first argument (%1) refers to a start time for an event,
#. while the second one (%2) refers to the end time
#: qml/EventBubble.qml:139
msgid "%1 - %2"
msgstr ""

#: qml/EventDetails.qml:37 qml/NewEvent.qml:596
msgid "Event Details"
msgstr ""

#: qml/EventDetails.qml:40
#, fuzzy
msgid "Edit"
msgstr "Edit"

#: qml/EventDetails.qml:173 qml/TimeLineHeader.qml:66 qml/calendar.qml:750
msgid "All Day"
msgstr ""

#: qml/EventDetails.qml:392
msgid "Attending"
msgstr ""

#: qml/EventDetails.qml:394
msgid "Not Attending"
msgstr ""

#: qml/EventDetails.qml:396
msgid "Maybe"
msgstr ""

#: qml/EventDetails.qml:398
msgid "No Reply"
msgstr ""

#: qml/EventDetails.qml:437 qml/NewEvent.qml:632
msgid "Description"
msgstr ""

#: qml/EventDetails.qml:464 qml/NewEvent.qml:867 qml/NewEvent.qml:884
msgid "Reminder"
msgstr ""

#. TRANSLATORS: this refers to how often a recurrent event repeats
#. and it is shown as the header of the page to choose repetition
#. and as the header of the list item that shows the repetition
#. summary in the page that displays the event details
#: qml/EventRepetition.qml:40 qml/EventRepetition.qml:179
msgid "Repeat"
msgstr ""

#: qml/EventRepetition.qml:199
msgid "Repeats On:"
msgstr ""

#: qml/EventRepetition.qml:245
msgid "Interval of recurrence"
msgstr ""

#: qml/EventRepetition.qml:270
msgid "Recurring event ends"
msgstr ""

#. TRANSLATORS: this refers to how often a recurrent event repeats
#. and it is shown as the header of the option selector to choose
#. its repetition
#: qml/EventRepetition.qml:294 qml/NewEvent.qml:840
msgid "Repeats"
msgstr ""

#: qml/EventRepetition.qml:320 qml/NewEvent.qml:494
msgid "Date"
msgstr ""

#. TRANSLATORS: the argument refers to multiple recurrence of event with count .
#. E.g. "Daily; 5 times."
#: qml/EventUtils.qml:75
msgid "%1; %2 time"
msgid_plural "%1; %2 times"
msgstr[0] ""
msgstr[1] ""

#. TRANSLATORS: the argument refers to recurrence until user selected date.
#. E.g. "Daily; until 12/12/2014."
#: qml/EventUtils.qml:79
msgid "%1; until %2"
msgstr ""

#: qml/EventUtils.qml:93
msgid "; every %1 days"
msgstr ""

#: qml/EventUtils.qml:95
msgid "; every %1 weeks"
msgstr ""

#: qml/EventUtils.qml:97
msgid "; every %1 months"
msgstr ""

#: qml/EventUtils.qml:99
msgid "; every %1 years"
msgstr ""

#. TRANSLATORS: the argument refers to several different days of the week.
#. E.g. "Weekly on Mondays, Tuesdays"
#: qml/EventUtils.qml:125
msgid "Weekly on %1"
msgstr ""

#: qml/LimitLabelModel.qml:25
msgid "Never"
msgstr ""

#: qml/LimitLabelModel.qml:26
msgid "After X Occurrence"
msgstr ""

#: qml/LimitLabelModel.qml:27
msgid "After Date"
msgstr ""

#. TRANSLATORS: This is shown in the month view as "Wk" as a title
#. to indicate the week numbers. It should be a max of up to 3 characters.
#: qml/MonthComponent.qml:294
msgid "Wk"
msgstr ""

#: qml/MonthView.qml:79 qml/WeekView.qml:140
msgid "%1 %2"
msgstr ""

#: qml/NewEvent.qml:206
msgid "End time can't be before start time"
msgstr ""

#: qml/NewEvent.qml:389 qml/NewEventBottomEdge.qml:54
msgid "New Event"
msgstr ""

#: qml/NewEvent.qml:419
msgid "Save"
msgstr ""

#: qml/NewEvent.qml:430
msgid "Error"
msgstr ""

#: qml/NewEvent.qml:432
msgid "OK"
msgstr ""

#: qml/NewEvent.qml:494
msgid "From"
msgstr ""

#: qml/NewEvent.qml:519
msgid "To"
msgstr ""

#: qml/NewEvent.qml:548
msgid "All day event"
msgstr ""

#: qml/NewEvent.qml:585
msgid "Event Name"
msgstr ""

#: qml/NewEvent.qml:605
msgid "More details"
msgstr ""

#: qml/NewEvent.qml:656
msgid "Location"
msgstr ""

#: qml/NewEvent.qml:744
msgid "Guests"
msgstr ""

#: qml/NewEvent.qml:754
msgid "Add Guest"
msgstr ""

#: qml/OnlineAccountsHelper.qml:39
msgid "Pick an account to create."
msgstr ""

#: qml/RecurrenceLabelDefines.qml:23
msgid "Once"
msgstr ""

#: qml/RecurrenceLabelDefines.qml:24
msgid "Daily"
msgstr ""

#: qml/RecurrenceLabelDefines.qml:25
msgid "On Weekdays"
msgstr ""

#. TRANSLATORS: The arguments refer to days of the week. E.g. "On Monday, Tuesday, Thursday"
#: qml/RecurrenceLabelDefines.qml:27
msgid "On %1, %2 ,%3"
msgstr ""

#. TRANSLATORS: The arguments refer to days of the week. E.g. "On Monday and Thursday"
#: qml/RecurrenceLabelDefines.qml:29
msgid "On %1 and %2"
msgstr ""

#: qml/RecurrenceLabelDefines.qml:30
msgid "Weekly"
msgstr ""

#: qml/RecurrenceLabelDefines.qml:31
msgid "Monthly"
msgstr ""

#: qml/RecurrenceLabelDefines.qml:32
msgid "Yearly"
msgstr ""

#: qml/RemindersModel.qml:31 qml/RemindersModel.qml:99
msgid "No Reminder"
msgstr ""

#. TRANSLATORS: this refers to when a reminder should be shown as a notification
#. in the indicators. "On Event" means that it will be shown right at the time
#. the event starts, not any time before
#: qml/RemindersModel.qml:34 qml/RemindersModel.qml:103
msgid "On Event"
msgstr ""

#: qml/RemindersModel.qml:43 qml/RemindersModel.qml:112
msgid "%1 week"
msgid_plural "%1 weeks"
msgstr[0] ""
msgstr[1] ""

#: qml/RemindersModel.qml:54 qml/RemindersModel.qml:110
msgid "%1 day"
msgid_plural "%1 days"
msgstr[0] ""
msgstr[1] ""

#: qml/RemindersModel.qml:65 qml/RemindersModel.qml:108
#: qml/SettingsPage.qml:274 qml/SettingsPage.qml:275 qml/SettingsPage.qml:276
#: qml/SettingsPage.qml:277
msgid "%1 hour"
msgid_plural "%1 hours"
msgstr[0] ""
msgstr[1] ""

#: qml/RemindersModel.qml:74 qml/SettingsPage.qml:270 qml/SettingsPage.qml:271
#: qml/SettingsPage.qml:272 qml/SettingsPage.qml:273
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] ""
msgstr[1] ""

#: qml/RemindersModel.qml:104 qml/RemindersModel.qml:105
#: qml/RemindersModel.qml:106 qml/RemindersModel.qml:107
#: qml/SettingsPage.qml:385 qml/SettingsPage.qml:386 qml/SettingsPage.qml:387
#: qml/SettingsPage.qml:388 qml/SettingsPage.qml:389 qml/SettingsPage.qml:390
#: qml/SettingsPage.qml:391 qml/SettingsPage.qml:392
msgid "%1 minutes"
msgstr ""

#: qml/RemindersModel.qml:109
msgid "%1 hours"
msgstr ""

#: qml/RemindersModel.qml:111
msgid "%1 days"
msgstr ""

#: qml/RemindersModel.qml:113
msgid "%1 weeks"
msgstr ""

#: qml/RemindersModel.qml:114
msgid "Custom"
msgstr ""

#: qml/RemindersPage.qml:62
msgid "Custom reminder"
msgstr ""

#: qml/RemindersPage.qml:73
msgid "Set reminder"
msgstr ""

#: qml/SettingsPage.qml:85
msgid "Show week numbers"
msgstr ""

#: qml/SettingsPage.qml:104
msgid "Display Chinese calendar"
msgstr ""

#: qml/SettingsPage.qml:125
msgid "Business hours"
msgstr ""

#: qml/SettingsPage.qml:232
msgid "Data refresh interval"
msgstr ""

#: qml/SettingsPage.qml:262
msgid ""
"Note: Automatic syncronisation currently only works while the app is open in "
"the foreground. Alternatively set background suspension to off. Then sync "
"will work while the app is open in the background."
msgstr ""

#: qml/SettingsPage.qml:316
msgid "Default reminder"
msgstr ""

#: qml/SettingsPage.qml:363
msgid "Default length of new event"
msgstr ""

#: qml/SettingsPage.qml:426
msgid "Default calendar"
msgstr ""

#: qml/SettingsPage.qml:495
msgid "Theme"
msgstr ""

#: qml/SettingsPage.qml:518
msgid "System theme"
msgstr ""

#: qml/SettingsPage.qml:519
msgid "SuruDark theme"
msgstr ""

#: qml/SettingsPage.qml:520
msgid "Ambiance theme"
msgstr ""

#. TRANSLATORS: W refers to Week, followed by the actual week number (%1)
#: qml/TimeLineHeader.qml:54
msgid "W%1"
msgstr ""

#: qml/WeekView.qml:147 qml/WeekView.qml:148
msgid "MMM"
msgstr ""

#: qml/YearView.qml:83
msgid "Year %1"
msgstr ""

#: qml/calendar.qml:99
msgid ""
"Calendar app accept four arguments: --starttime, --endtime, --newevent and --"
"eventid. They will be managed by system. See the source for a full comment "
"about them"
msgstr ""

#: qml/calendar.qml:381 qml/calendar.qml:575
msgid "Day"
msgstr ""

#: qml/calendar.qml:389 qml/calendar.qml:596
msgid "Week"
msgstr ""

#: qml/calendar.qml:397 qml/calendar.qml:617
msgid "Month"
msgstr ""

#: qml/calendar.qml:405 qml/calendar.qml:638
msgid "Year"
msgstr ""
